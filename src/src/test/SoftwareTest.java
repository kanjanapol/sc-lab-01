package src.test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import src.model.Card;

import src.gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(600, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		Card card1 = new Card();
		frame.setResult("credit "+card1.getcredit());
		card1.addcredit(500);
		frame.extendResult("addmoney "+card1.getcredit());
		
		card1.buyfood(200);
		frame.extendResult("buyfood "+card1.getc());
		
		
		
		
		frame.extendResult("credit  "+card1.getcredit());

	}

	ActionListener list;
	SoftwareFrame frame;
}
